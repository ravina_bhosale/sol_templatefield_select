﻿using Sol_TemplateFiled_Select.DAL.ORD;
using Sol_TemplateFiled_Select.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_TemplateFiled_Select.DAL
{
    public class PersonDal
    {
        #region Declaration
        private PersonDcDataContext dc = null;
        #endregion

        #region Constructor
        public PersonDal()
        {
            dc = new PersonDcDataContext();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<PersonEntity>> GetPersonData()
        {
            return await Task.Run(() =>
            {
                var getQuery =
                dc
                ?.Persons
                ?.AsEnumerable()
                ?.Select((lelistPersonObj) => new PersonEntity()
                {
                    BusinessEntityId = lelistPersonObj.BusinessEntityID,
                    FirstName = lelistPersonObj.FirstName,
                    MiddleName = lelistPersonObj.MiddleName,
                    LastName = lelistPersonObj.LastName

                }).ToList();

                return getQuery;

            });
        }


        public async Task<int?> GetPersonDataCount()
        {
            return await Task.Run(async () =>
            {

                return

                        (await this.GetPersonData())
                        ?.AsEnumerable()
                        ?.Count();


            });
        }

        /// <summary>
        /// For Pagination
        /// </summary>
        /// <param name="startRowIndex"></param>
        /// <param name="maximumSize"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PersonEntity>> GetPersonDataPagination(int startRowIndex, int maximumSize)
        {
            return await Task.Run(async () =>
            {

                return
                    (
                        await this.GetPersonData())
                        ?.AsEnumerable()
                        ?.Skip(startRowIndex)
                        ?.Take(maximumSize)
                        ?.ToList();

            });
        }

        #endregion

    }
}