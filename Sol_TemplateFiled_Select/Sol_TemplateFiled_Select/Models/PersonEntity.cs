﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_TemplateFiled_Select.Models
{
    public class PersonEntity
    {
        public decimal? BusinessEntityId { get; set; }

        public String FirstName { get; set; }

        public String MiddleName { get; set; }

        public String LastName { get; set; }
    }
}