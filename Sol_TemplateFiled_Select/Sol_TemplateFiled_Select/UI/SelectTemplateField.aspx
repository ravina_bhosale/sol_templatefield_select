﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="SelectTemplateField.aspx.cs" Inherits="Sol_TemplateFiled_Select.UI.SelectTemplateField" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="scriptManager" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvPerson" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="#FFCCFF" BorderColor="Black" BorderStyle="Solid" DataKeyNames="BusinessEntityId" GridLines="Vertical" OnRowCommand="gvPerson_RowCommand" ItemType="Sol_TemplateFiled_Select.Models.PersonEntity" SelectMethod="BindPersonData" >
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:Button ID="btnSelect" Text="Select" runat="server" CommandName="Select" CommandArgument="<%#((GridViewRow) Container).RowIndex %>" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FirstName">
                             <ItemTemplate>
                                    <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
                                </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MiddleName">
                            <ItemTemplate>
                                <asp:Label ID="lblMiddleName" runat="server" Text='<%#Eval("MiddleName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
       
                        <asp:TemplateField HeaderText="LastName">
                            <ItemTemplate>
                                <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
