﻿using Newtonsoft.Json;
using Sol_TemplateFiled_Select.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_TemplateFiled_Select.UI
{
    public partial class SelectTemplateField : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void gvPerson_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            await Task.Run(() =>
            {
                if (e.CommandName == "Select")
                {
                    // Get Row Index
                    var rowIndex = Convert.ToInt32(e.CommandArgument);

                    // Create an Instance of Person Entity
                    var personEntityObj = new PersonEntity()
                    {
                        BusinessEntityId = Convert.ToDecimal(gvPerson.DataKeys[rowIndex].Value),
                        FirstName = ((Label)gvPerson.Rows[rowIndex].Cells[1].FindControl("lblFirstName")).Text,
                        MiddleName = ((Label)gvPerson.Rows[rowIndex].Cells[1].FindControl("lblMiddleName")).Text,
                        LastName = ((Label)gvPerson.Rows[rowIndex].Cells[2].FindControl("lblLastName")).Text
                    };

                   

                }
            });
           
        }
    }
}