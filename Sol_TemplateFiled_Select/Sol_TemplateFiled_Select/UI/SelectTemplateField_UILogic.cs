﻿using Sol_TemplateFiled_Select.DAL;
using Sol_TemplateFiled_Select.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_TemplateFiled_Select.UI
{
    public partial class SelectTemplateField
    {
        #region Declaration
        private PersonDal personDalObj = null;
        #endregion

        #region Public Method
        public async Task<SelectResult> BindPersonData(int startRowIndex, int maximumRows)
        {
            // Create an Instance of Person Dal
            personDalObj = new PersonDal();

            // send result to gridview
            return
                   new SelectResult(
                        Convert.ToInt32(await personDalObj?.GetPersonDataCount()),
                        await personDalObj?.GetPersonDataPagination(startRowIndex,maximumRows)
                       );
        }

       
            #endregion
        }
}